#605.484 Summer 2016
#Team5-summer16
#Members: Sajjad Syed, Chin-Ting Ko
#Assignment 3

#Filename: db_generator.rb
# Read in the CSV file using Ruby's CSV library. Create an array 
# of students, where each student will have its fields populated 
# based on the values of the *.csv file with the exception of 
# student_id, gpa, and taking_courses fields.

require 'csv'
require_relative 'student'
require_relative 'course'
require 'yaml'


students_csv_text = File.read('students.csv')
students_csv_data = CSV.parse(students_csv_text, :headers => true)
students_array=[]  # create student array
student_id =1 # initial student id
random_gpa= Random.new


course1 = Course.new("Intro to Java", 605.401)
course2 = Course.new("Intro to Ruby", 605.402)
course3 = Course.new("Intro to Rails", 605.403)
course4 = Course.new("Ruby On Rails", 605.484)
taking_courses= [course1, course2, course3, course4]


students_csv_data.each do |row| # import student_csv_data to student_array
  course_number= Random.new.rand(0...4) # random choose course numbers from 0~4

  students_array.push(Student.new do |s| # assign row data to student class
    s.student_id = student_id
    s.first_name = row[0]
    s.last_name  = row[1]
    s.email = row[6]
    s.city = row[3]
    s.state = row[4]
    s.gender = row[7]
    s.gpa = random_gpa.rand(2.0...4.0) # assign random floating gpa
    s.pounds = row[8].to_f  # save as float point number
    s.taking_courses = taking_courses.sample(course_number) #randomly assign courses to each student
  end)

  student_id+=1

end

#Serialize the students array to university_db.yml using Ruby's YAML library
File.open("university_db.yml", "w+") { |f| YAML.dump(students_array, f) }

course_number= Random.new.rand(0...4)
puts course_number
puts taking_courses.sample(course_number)
puts students_array[4]