#605.484 Summer 2016
#Team5-summer16
#Members: Sajjad Syed, Chin-Ting Ko
#Assignment 3

#Filename: student.rb
# contains the following fields (with appropriate read/write accessors): 
# student_id, first_name, last_name, city, state, email, gender, pounds, 
# gpa and taking_courses. The Student class had an empty constructor, 
# but provides an ability to be initialized via a block initializer.

class Student
  
  # getters and setters for course name and course id
  attr_accessor :student_id, :first_name, :last_name, :city, :state, 
    :email, :gender, :pounds, :gpa, :taking_courses   
  
  
  # Empty CONSTRUCTOR but with ability to be initialized 
  # via a block initializer
  def initialize      
    yield self if block_given?    
  end
  
  
  #override the to_s method
  def to_s      
    "Name: #{@first_name} #{@last_name}, Student ID: #{@student_id}, EMAIL: #{@email}, (#{@gender} from #{@city}, #{@state}. Weight: #{@pounds}, GPA: #{@gpa}) \n    Courses: #{@taking_courses}"
  end
  
end
