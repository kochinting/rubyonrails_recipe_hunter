
#605.484 Summer 2016
#Team5-summer16
#Members: Sajjad Syed, Chin-Ting Ko
#Assignment 3

#Filename: db_api.rb
#Serves as an API/DSL for selecting records from the 
#'YAML-base' based on certain conditions.

require_relative 'student'
require_relative 'course'
require 'yaml'


class DbApi
  
  def self.students  
    #load YAML file only when instance variable student_data is nil
    @students_data = YAML.load_file "university_db.yml" if @students_data.equal?(nil)   
  end

  def self.select_by_gender (gender) 
    self.students
    @students_data.select{|a| a.gender == gender} #return select by gender
  end

  def self.select_by_first_name (first_name)
    self.students
    @students_data.select{|a| a.first_name =~ first_name} #return select by first name
  end

  def self.select_by_last_name (last_name)
    self.students
    @students_data.select{|a| a.last_name =~ last_name} #return select by last name
  end

  def self.select_by_weight_more_than(pounds)
    self.students
    @students_data.select{|a| a.pounds > pounds}  #return select by more than pounds
  end
end







