#605.484 Summer 2016
#Team5-summer16
#Members: Sajjad Syed, Chin-Ting Ko
#Assignment 3

#Filename: db_generator.rb
# Read in the CSV file using Ruby's CSV library. Create an array 
# of students, where each student will have its fields populated 
# based on the values of the *.csv file with the exception of 
# student_id, gpa, and taking_courses fields.