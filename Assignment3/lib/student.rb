#605.484 Summer 2016
#Team5-summer16
#Members: Sajjad Syed, Chin-Ting Ko
#Assignment 3

#Filename: student.rb
# contains the following fields (with appropriate read/write accessors): 
# student_id, first_name, last_name, city, state, email, gender, pounds, 
# gpa and taking_courses. The Student class had an empty constructor, 
# but provides an ability to be initialized via a block initializer.

class Student
  
  # getters and setters for course name and course id
  attr_accessor :student_id, :first_name, :last_name, :city, :state, 
    :email, :gender, :pounds, :gpa, :taking_courses   
  
  
  # Empty CONSTRUCTOR
  #def initialize ()
  #  @student_id = nil 
  #  @first_name = nil 
  #  @last_name = nil
  #  @city = nil
  #  @state = nil
  #  @email = nil
  #  @gender = nil
  #  @pounds = nil
  #  @gpa = nil
  #  @taking_courses = nil
  #end
  
  # Empty CONSTRUCTOR but with ability to be initialized 
  # via a block initializer
  def initialize      
    yield self if block_given?    
  end
  
  
  #override the to_s method
  def to_s      
    "Name: #{@first_name} #{@last_name}, Student ID: #{@student_id}, EMAIL: #{@email} (#{@gender} from #{@city}, #{@state}. Weight: #{@pounds}, GPA: #{@gpa} \nCourses: #{@taking_courses}"
  end
  
end


s1 = Student.new do |s|
  s.student_id = 123
  s.first_name = "John"
  s.last_name  = "Doe"
  s.email = "johndoe@gmail.com"
  s.city = "germantown"
  s.state = "MD"
  s.gender = "Male"
  s.gpa = 3.4
  s.pounds = 230
  s.taking_courses = "None"
end

puts s1