
#605.484 Summer 2016
#Team5-summer16
#Members: Sajjad Syed, Chin-Ting Ko
#Assignment 3

#Filename: course.rb
#contains course_name and course_id fields which 
#are set via the class constructor. 

class Course
  
  #getters and setters for course name and course id
  attr_accessor :course_name, :course_id  
  
  
  #CONSTRUCTOR
  def initialize (coursename, courseid)       
    @course_name = coursename      
    @course_id = courseid
  end
  
  
  #override the to_s method
  def to_s      
    "#{@course_id} - #{@course_name}"
  end
  
end

c1 = Course.new("Ruby On Rails", 605.471)
puts c1




